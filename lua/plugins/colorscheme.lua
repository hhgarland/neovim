-- return {
--   -- add colorscheme
--   {
--     "daschw/leaf.nvim",
--     opts = {
--       colors = { bg0 = "#333333" }, -- match pop os terminal background
--     },
--   },

return {
  -- add colorscheme
  { "Mofiqul/vscode.nvim" },

  -- Configure LazyVim to load colorscheme
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "vscode",
    },
  },
}
